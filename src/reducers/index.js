import { combineReducers } from 'redux'
import messages from './messages'
// import visibilityFilter from './visibilityFilter'

export default combineReducers({
  messages
  // visibilityFilter
})

