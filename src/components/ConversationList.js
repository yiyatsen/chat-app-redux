import React, { Component } from "react";
import { connect } from 'react-redux';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

class ConversationList extends Component {
  list = {
    messages: []
  }

  // componentWillReceiveProps(nextProps) {
  //   this.setState({ messages: nextProps.messages })
  // }

  render() {
    return (
      <List id="message_block" dense={true}>
        {this.list.messages.map(dialog => (
          <ListItem key={dialog.id}>
            <ListItemText primary={dialog.text} />
          </ListItem>
        ))}
      </List>
    );
  }
}

const mapStateToProps = state => ({
  list: state
})

const mapDispatchToProps = dispatch => ({
})

export default connect(
  mapStateToProps,
  mapDispatchToProps)(ConversationList);
