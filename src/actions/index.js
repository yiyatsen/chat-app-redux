
export const chatMessage = text => ({
  type: 'CHAT_MESSAGE',
  text
})

export const addUser = user => ({
  type: 'ADD_USER',
  user
})

export const userLeft = text => ({
  type: 'USER_LEFT',
  text
})

// export const setVisibilityFilter = filter => ({
//   type: 'SET_VISIBILITY_FILTER',
//   filter
// })

// export const toggleTodo = id => ({
//   type: 'TOGGLE_TODO',
//   id
// })

// export const VisibilityFilters = {
//   SHOW_ALL: 'SHOW_ALL',
//   SHOW_COMPLETED: 'SHOW_COMPLETED',
//   SHOW_ACTIVE: 'SHOW_ACTIVE'
// }
