import React, { Component } from 'react';
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { chatMessage, addUser, userLeft } from './actions'
import NavBar from './components/NavBar';
import ConversationList from './components/ConversationList';
import LoginDialog from './components/LoginDialog';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import './App.css';

const io = require('socket.io-client')
const socket = io.connect('http://localhost:3001')
const chatroom_title = "Socket.io聊天室"

class App extends Component {
  // constructor(props) {
  //   super(props);
  // };

  state = {
    user: '',
    message: '',
    open: true,
    dense: true //,
    // messages: []
  };

  componentDidMount() {
    socket.on('chat message', (data) => {
      const state = this.state;
      const msg = "[" + data.username + "]發言:" + data.msg;
      state.messages.push({ text: msg, id: this.state.messages.length });
      state.message = "";
      this.setState(state);
    });

    socket.on('add user', (data) => {
      console.log("dispatch(addUser(data.username))");
      this.props.store.dispatch(addUser(data.username))
      // const msg = "[" + data.username + "] 已加入";
      // const state = this.state;
      // state.messages.push({ text: msg, id: this.state.messages.length });
      // this.setState(state);
    });

    socket.on('user left', (data) => {
      const msg = "[" + data.username + "] 已離開";
      const state = this.state;
      state.messages.push({ text: msg, id: this.state.messages.length });
      this.setState(state);
    });
  }

  handleLoginClose = (user) => {
    socket.emit("add user", user);
    this.setState({ user });
  };

  handleKeyDown = (e) => {
    if (e.keyCode === 13) {
      this.handleSendMessageClick();
    }
  }

  handleSendMessageClick = () => {
    socket.emit('chat message', this.state.message);
  };

  render() {
    return (
      <div>
        <NavBar
          chatroom_title={chatroom_title}
          user={this.state.user}
        />

        <ConversationList
          dense={this.state.dense}
          messages={this.state.messages}
        />

        <TextField
          id="message-input"
          type="text"
          value={this.state.message}
          onKeyDown={this.handleKeyDown}
          onChange={e => this.setState({ message: e.target.value })}
        />

        <Button
          className="message-input"
          id="send"
          variant="contained"
          color="primary"
          onClick={this.handleSendMessageClick}
        >
          發言
        </Button>

        <LoginDialog
          open={this.state.open}
          chatroom_title={chatroom_title}
          onLoginClose={this.handleLoginClose}
        />
      </div>
    );
  }
}

App.propTypes = {
  messages: PropTypes.array.isRequired
  // onIncrement: PropTypes.func.isRequired,
  // onDecrement: PropTypes.func.isRequired
}

export default connect()(App);
